import sqlite3


class Banco():

    def __init__(self):

        self.conexao = sqlite3.connect("/home/eliseu/banck")
        self.cursor = self.conexao.cursor()

    def inserir_cliente(self, nome, tel):
        
        self.cursor.execute("INSERT INTO cliente (nome, tel) VALUES (?, ?)", (nome, tel))
        self.conexao.commit()

        return print("inserido com sucesso!")

    def consultar_cliente(self):

        self.cursor.execute("SELECT * FROM cliente")

        rows = self.cursor.fetchall()
        # Exibindo os dados
        if not rows:
            print("Ainda não há cliente cadastrados. \n")
        else:
                for row in rows:
                    print(row)
    def deletar_cliente(self, id):

        self.cursor.execute("DELETE FROM conta WHERE titular = ?", (id,))
        self.cursor.execute("DELETE FROM cliente WHERE id = ?", (id,))
        resultado = self.conexao.commit()
        
        if not resultado:
            return print("conta deletado com sucesso!")
        else:
            print("Parece que este usuário não existe")
    
    def update_cliente(self, nome, tel, id):

        self.cursor.execute("UPDATE cliente SET nome = ?, tel = ? WHERE id = ?", (nome, tel, id))
        self.conexao.commit()

        return print("atualizado com sucesso!")
    
    def inserir_conta(self, titular, saldo):
        self.cursor.execute("INSERT INTO conta (titular, saldo) VALUES (?, ?)", (titular, saldo))
        self.conexao.commit()

        return print("conta criada com sucesso!")
    
    def atualizar(self, saldo, titular):
        self.cursor.execute("UPDATE conta SET saldo = ? WHERE titular = ?", (saldo, titular))
        self.conexao.commit()

        return print("Operação feita com sucesso!")
    
    def consulta(self, titular):
        #SELECT cliente.nome, conta.saldo
        #FROM cliente
        #JOIN conta ON cliente.id = conta.titular;

        #self.cursor.execute("SELECT saldo FROM conta WHERE titular = ?", (titular, ))
        #saldo = self.cursor.fetchone()
        self.cursor.execute("SELECT cliente.nome, conta.saldo FROM cliente JOIN conta ON cliente.id = conta.titular WHERE cliente.id = ?", (titular, ))
        resultado = self.cursor.fetchone()
        
        return resultado[0], resultado[1]
       
    
        # Fechando a conexão
        self.conexao.close()