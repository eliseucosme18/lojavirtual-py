class Conta:

    def __init__(self, titular, saldo):
        self._saldo = saldo
        self._titular = titular

    def get_titular(self):
        return self._titular


    def get_saldo(self):
        return self._saldo
    

    
    def set_saldo(self, saldo):
        if(saldo<0):
            print("O saldo não pode ser negativo")
        else:
            self._saldo = saldo


    
    def saque(self, valor):
        if (self._saldo>=valor):
            self._saldo-=valor
            print("Saque realizado com sucesso!")
        else:
            print("Saldo insuficiente")




    
    def depositar(self, valor):
        self._saldo += valor
        return self._saldo





    def extrato(self, titular):
        return self._titular