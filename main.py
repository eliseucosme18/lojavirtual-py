class main:

    pass

from bd import Banco
from Cliente import Cliente
from Conta import Conta

banco = Banco()

entrada = int(input("MENU: \n"+"1 - ABA CLIENTE \n"+"2 - ABA CONTA \n"+"3 - SAIR \n"))

while entrada != 3:

    if entrada == 1:
        print("\n")
        entrada = int(input("ABA CLIENTE: \n \n"+"1 - Novo cliente \n"+
        "2 - Consultar cliente \n"+"3 - Atualizar cliente \n"+"4 - Deletar cliente \n"+"5 - Sair \n"))

        while entrada != 5:

            if entrada == 1:
                nome = input("Digite seu nome: \n")
                telefone = int(input("Digite o telefone: \n"))
                cliente = Cliente(nome, telefone)
                banco.inserir_cliente(cliente.get_nome(), cliente.get_telefone())
            
            elif entrada == 2:
                banco.consultar_cliente()
            elif entrada == 3:
                nome = input("Informe o novo nome: \n")
                telefone = int(input("Informe o novo telefone: \n"))
                id = int(input("Informe o id do cliente que precisa ser atualizado: \n"))
                cliente = Cliente(nome, telefone)
                banco.update_cliente(cliente.get_nome(), cliente.get_telefone(), id)
            elif entrada == 4:
                id = int(input("Informe o ID do cliente que irá ser deletado: \n"))
                banco.deletar_cliente(id)
            
            elif entrada == 5:
                print("finalizando processo")

            entrada = int(input("ABA cliente: \n \n"+"1 - Novo cliente \n"+
            "2 - Consultar cliente \n"+"3 - Atualizar cliente \n"+"4 - Deletar cliente \n"+"5 - Sair \n"))



    if entrada == 2:

        entrada = int(input("ABA CONTA: \n \n"+"1  - Criar conta \n"+"2 - Depositar \n"+"3 - Sacar \n"+"4 - Extrato \n"+"5 - Sair \n"))

        while entrada != 5:

            if entrada == 1:
                titular = int(input("Informe o titular da conta: \n"))
                saldo = float(input("Deposite um valor para abertura da conta: \n"))
                conta = Conta(titular, saldo)
                banco.inserir_conta(conta.get_titular(), conta.get_saldo())
            
            elif entrada == 2:
                titular = int(input("Informe o ID titular que deseja fazer o deposito \n"))
                nome, saldo = banco.consulta(titular)
                print("Olá ",nome,"! Seu saldo é de: R$",saldo,". \n")
                conta = Conta(nome,  saldo)
                valor = float(input("Informe o valor do deposito: \n"))
                conta.depositar(valor)
                #print(saldoatual)
                
                print(conta.get_saldo())
                print(conta.get_titular())
                
                banco.atualizar(conta.get_saldo(), titular)
            
            elif entrada == 3:
                titular = int(input("Informe o ID titular que deseja fazer o saque \n"))
                nome, saldo = banco.consulta(titular)

                print("Olá ",nome,"! Seu saldo é de: R$",saldo,". \n")
                conta = Conta(nome,  saldo)
                valor = float(input("Informe o valor do saque: \n"))
                
                conta.saque(valor)

                print(conta.get_saldo())
                print(conta.get_titular())

                banco.atualizar(conta.get_saldo(), titular)
            
            elif entrada == 4:
                titular = int(input("Informe o ID titular que deseja tirar o extrato \n"))
                nome, saldo = banco.consulta(titular)

                print("Olá ",nome,"! Seu saldo é de: R$",saldo,". \n")

            elif entrada == 5:
                print("Retornando para menu principal")
            
            
            entrada = int(input("ABA CONTA: \n \n"+"1  - Criar conta \n"+"2 - Depositar \n"+"3 - Sacar \n"+"4 - Extrato \n"+"5 - Sair \n"))


    if entrada == 3:
        print("Finalizando processo.")

    entrada = int(input("MENU: \n"+"1 - ABA CLIENTE \n"+"2 - ABA CONTA \n"+"3 - SAIR \n"))
